#pragma once

#ifdef _WIN32
#include  <Windows.h>
#include <stdlib.h>
#define timeval timeval64
struct timeval64
{
  time_t tv_sec;
  time_t tv_usec;
};

struct timezone 
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};

 
int gettimeofday(struct timeval *tv, struct timezone *tz);
#define __thread __declspec(thread)
#define sleep(ms) Sleep(ms)

inline struct tm* localtime_r(const time_t *timep, struct tm *result)
{
  errno_t err = localtime_s(result, timep);
  if (0 == err)
    return result;
  char* errstr = strerror(err);
  printf("%s", errstr);
  return NULL;
}

inline struct tm* localtime_r(const __time32_t *timep, struct tm *result)
{
  if (0 == _localtime32_s(result, timep))
    return result;
  return NULL;
}
#define snprintf _snprintf

inline errno_t rand_r(unsigned int* r)
{
  return rand_s(r);
}

#endif
