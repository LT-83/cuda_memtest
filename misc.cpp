
#include <stdlib.h>
#include <assert.h>
#include "cuda_memtest.h" 

#ifdef _WIN32
	#include <direct.h>
#endif

// volatile int intake_temp[MAX_GPU_NUM/2];
volatile int gpu_temp[MAX_GPU_NUM];

#define NVSMI_PATH "C:\\Program Files\\NVIDIA Corporation\\NVSMI"
#define NV_TEMP_WINDOWS "nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader"
void update_temperature(int num_gpus)
{
#ifdef _WIN32
	char cwd[MAX_PATH];
	_getcwd(cwd, MAX_PATH);

	if (_chdir(NVSMI_PATH)) {
		printf("ERROR: Unable to locate nvidia-smi directory to get NVIDIA card temperatures\n");
		exit(ERR_GENERAL);
	}
	
	FILE* file = _popen(NV_TEMP_WINDOWS, "r");
	if (file == NULL) {
		PRINTF("ERROR: Executing nvidia-smi to get NVIDIA card temperatures");
		exit(ERR_GENERAL);
	}
#else
	FILE* file = popen("nvidia-smi|/bin/grep Temperature|gawk '{print $3 $4}'", "r");

	if (file == NULL) {
		printf("ERROR: opening pipe failed\n");
		exit(ERR_GENERAL);
	}
#endif

	unsigned int i = 0;

	char buf[256];
	char* s;
	int gpu_0, gpu_1, gpu_2, gpu_3;

	s = fgets(buf, 256, file);
	if (s == NULL) {
		PRINTF("ERROR: Unable to acquire temperatures from nvidia-smi output\n");
		exit(ERR_GENERAL);
	}
	sscanf(buf, "%dC", &gpu_0);
	gpu_temp[0] = gpu_0;

	if (num_gpus > 1) {
		s = fgets(buf, 256, file);
		sscanf(buf, "%dC", &gpu_1);
		gpu_temp[1] = gpu_1;
	}

	if (num_gpus > 2) {
		s = fgets(buf, 256, file);
		sscanf(buf, "%dC", &gpu_2);
		gpu_temp[2] = gpu_2;
	}

	if (num_gpus > 3) {
		s = fgets(buf, 256, file);
		sscanf(buf, "%dC", &gpu_3);
		gpu_temp[3] = gpu_3;
	}

	DEBUG_PRINTF("temperature updated: %d %d %d %d \n",
		gpu_temp[0], gpu_temp[1], gpu_temp[2], gpu_temp[3]);

#ifdef _WIN32
  _pclose(file);
#else
  pclose(file);
#endif

}


unsigned long long
get_serial_number(void)
{   
#ifndef _WIN32
    FILE* file= popen("nvidia-smi|/bin/grep \"Serial Number\"|gawk '{print $4}'", "r");
    if (file == NULL){
	PRINTF("Warning: opening pipe failed for getting serial nubmer\n");
	return 0;
    }
    
    char buf[256];
    char* s;
    int t;
    unsigned long long sn;
    
    s = fgets(buf, 256, file);
    if (s == NULL){
	PRINTF("Warning: Getting serial number failed\n");
	pclose(file);
	return 0;
    }
    sscanf(buf, "%llu", &sn);
    
    PRINTF("Unit serial number: %llu\n", sn);
    
    pclose(file);
    
    return sn;
#else
  return 0;
#endif
}


#define NV_DRIVER_VER_LINUX "/proc/driver/nvidia/version"
#define NV_DRIVER_VER_WINDOWS "nvidia-smi -i 0 --query-gpu=driver_version --format=csv,noheader"
void
get_driver_info(char* info, unsigned int len)
{

#ifdef _WIN32

	char cwd[MAX_PATH];
	_getcwd(cwd, MAX_PATH);

	if (_chdir(NVSMI_PATH)) {
		printf("Unable to locate nvidia-smi directory to get NVIDIA driver version\n");
		info[0] = 0;
		return;
	}
	else
	{

		FILE* file = _popen(NV_DRIVER_VER_WINDOWS, "r");
		if (file == NULL) {
			PRINTF("Warning: Executing nvidia-smi to get NVIDIA driver version number failed\n");
			info[0] = 0;
			_pclose(file);
			return;
		}

		char buf[7];
		char* s;

		s = fgets(buf, 7, file);
		if (s == NULL) {
			PRINTF("Warning: Getting NVIDIA driver version number failed\n");
			info[0] = 0;
			_pclose(file);
			return;
		}

		info = s;
		PRINTF("NVIDIA driver version: %s\n", info);

		_pclose(file);

	}

#else

	FILE* file = fopen(NV_DRIVER_VER_LINUX, "r");
	if (file == NULL) {
		PRINTF("Warning: Opening %s failed\n", NV_DRIVER_VER_LINUX);
		info[0] = 0;
		return;
	}

	if (fgets(info, len, file) == NULL) {
		PRINTF("Warning: reading file failed\n");
		info[0] = 0;
		return;
	}

	PRINTF("NVIDIA driver version: %s\n", info);

	return;

#endif

	}
