Based on the code modified in the post here:
https://sourceforge.net/p/cudagpumemtest/discussion/960739/thread/80411958/#e0f6

The code has not been modified much other than to report the correct NVIDIA driver version via
a call to nvidia-smi on Windows and tested to compile under Windows 10 x64 w/ Visual Studio 2015
under CUDA 10.

Pre-compiled x64 binary includes support from SM_30 to SM_75
